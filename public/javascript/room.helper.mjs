import { socket } from "./game.mjs";

const username = sessionStorage.getItem("username");

const readyButton = document.querySelector(".ready-btn");
const disconnectButton = document.querySelector(".disconnect-btn");

readyButton.addEventListener("click", () => {
  socket.emit("USER_SET_READINESS", username);
});

disconnectButton.addEventListener("click", () => {
  socket.emit("disconnect", username);
});

export const userAlreadyActive = (username) => {
  window.location.replace("/login");
  sessionStorage.removeItem("username");
  alert(`Username ${username} is already taken`);
};

export const roomUpdate = (players, playersWrap) => {
  playersWrap.innerHTML = "";
  players.map((item) => {
    if (item.isReady) {
      playersWrap.innerHTML +=
        item.username === username
          ? `<div><div class='players-readiness player-ready is-ready'></div><p class='player-name'>${item.username}(you)</p></div>`
          : `<div><div class='players-readiness player-ready is-ready'></div><p class='player-name'p>${item.username}</p></div>`;
    } else {
      playersWrap.innerHTML +=
        item.username === username
          ? `<div><div class='players-readiness player-ready is-not-ready'></div><p class='player-name'>${item.username}(you)</p></div>`
          : `<div><div class='players-readiness player-ready is-not-ready'></div><p class='player-name'p>${item.username}</p></div>`;
    }
  });
};
