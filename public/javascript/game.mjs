import { userAlreadyActive, roomUpdate } from "./room.helper.mjs";
const playersList = document.querySelector(".players");

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { username } });

socket.on("USERNAME_ALREADY_ACTIVE", (username) => userAlreadyActive(username));
socket.on("UPDATE_ROOM", (players) => roomUpdate(players, playersList));
