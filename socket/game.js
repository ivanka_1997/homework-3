const users = [];

export default (socket, io) => {
  const username = socket.handshake.query.username;
  const found = users.some((item) => item.username === username);
  if (found) {
    console.log("username is taken");
    socket.emit("USERNAME_ALREADY_ACTIVE", username);
    return;
  } else {
    users.push({ username, isReady: false, finishTime: 0, symbolsTyped: 0 });
    io.emit("UPDATE_ROOM", users);
  }

  socket.on("disconnect", (username) => disconnectUser(username));

  socket.on("USER_SET_READINESS", (username) => userSetReadiness(username));

  function userSetReadiness(username) {
    let index = users.findIndex((x) => x.username === username);
    users[index].isReady = !users[index].isReady;
    io.emit("UPDATE_ROOM", users);
  }

  function disconnectUser(username) {
    console.log("disconct");

    let index = users.findIndex((x) => x.username === username);
    console.log(users[index]);
    users.splice(users[index], 1);
    console.log(users[index]);
    io.emit("UPDATE_ROOM", users);
  }
};
